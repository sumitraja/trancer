module DecodeEncodeTest;

import containers.inputcontainer;
import containers.outputcontainer;
import streams;
import containers.container:Event;
import std.string;
import ffmpeg.libavutil.samplefmt;
import utils;

class DecodeEncodeTest {
  this() {
    auto ic = new InputContainer("/home/mediabag/Videos/Serenity - HD DVD Trailer.mp4");
    auto oc = new OutputContainer("webm", "/tmp/webm.webm");
    auto stream = ic.audioStreams[0];
    auto as = oc.addAudioStream("libvorbis", "flt", stream.channels, stream.sampleRate);
    ic.addListener(new EncodingListener(stream, as, oc));

    bool eof = false;
    do{
      eof = ic.extractNextPacket();
    } while(!eof);
  }
}

class EncodingListener : InputEventListener {
  import std.stdio;
  FFmpegStream destStream;
  OutputContainer oc;

  this(FFmpegStream sourceStream, FFmpegStream destStream, OutputContainer oc) {
    super(sourceStream);
    this.destStream = destStream;
    this.oc = oc;
  }

  override void eventReceived(ref Event event) {
    auto opt = event.decodedPacket;
    if(opt.defined) {
      auto frame = opt.value;
//      ubyte*audio_data;
//      int lineSize;
//      ffmpegCall ({
//        return av_samples_alloc(&audio_data, lineSize, event.stream.ffCodecContext.channels, frame.nb_samples, destStream.ffCodecContext.sample_fmt, 0);
//      });
//      ffmpegCall ({
//        return av_samples_copy(&audio_data, frame.data, 0, 0, frame.nb_samples, frame.nb_channels, destStream.ffCodecContext.sample_fmt, 0);
//       });
      auto packet = destStream.encode(frame);
      if (packet.defined) {
        oc.writeFrame(packet.value);
      }
    }
  }
}

unittest {
  new DecodeEncodeTest();
}
