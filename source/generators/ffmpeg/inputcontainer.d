/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 
 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
module generators.ffmpeg.inputcontainer;

import std.string;
import std.conv;
import core.stdc.errno;
import pipeline.elements;
import pipeline.types;
import utils;
import logging.logger;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import scaffold.ffmpeg.context;
import scaffold.ffmpeg.events;

class InputContainer : Generator {

    this(ref FFmpegTrancerDecoderContext context, Processor next, ulong maxPackets = ulong.max) {
        super(next);
        mContext = context;
        log = Logger("InputContainer");
        mMaxPackets = maxPackets;
    }
    
    override Variant generate() {
        AVPacket pkt;
        auto fc = mContext.formatContext;
        int ret = EAGAIN;
        while(ret == EAGAIN) {
            ret = ffmpegCall({
                scope(failure) av_free_packet(&pkt);
                return av_read_frame(&fc, &pkt);
            }, [EAGAIN, AVERROR_EOF]);
            eof = ret == AVERROR_EOF;
            log("ret=" ~ to!string(ret));
            if(eof) {
                return Variant(new InputExhaustedEvent(mContext));
            }
        }
        auto event = Variant(new PacketEvent(pkt, mContext));
        mMaxPackets--;
        return event;
    }
    
    override @property bool shouldGenerate() {
        return !eof && (mMaxPackets > 0);
    }
    
private:
    Logger log;
    bool eof = false;
    FFmpegTrancerDecoderContext mContext;
    ulong mMaxPackets = 0;
}

unittest {
    import std.stdio;
    import std.conv;
    auto complete = false;

    class PacketLogger : Terminator {
        FFmpegTrancerDecoderContext mContext;
        
        this(ref FFmpegTrancerDecoderContext context) {
            this.mContext = context;
        }
        
        override void processImpl(ref Variant payload) 
        in {
            assert(payload.convertsTo!PacketEvent());
        } body {
            auto packetEvent = payload.get!PacketEvent();
            auto pkt = packetEvent.avPacket;
            auto stream = mContext.streamForPacket(pkt);
            assert(pkt.dts == 1);
            //assert(pkt.pts == 1);
            assert(stream.streamType == StreamType.VIDEO);
            complete = true;
        }
    }
    
    av_register_all();
    ulong pts = 0;
    auto context = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    assert(context.streams.length == 2, to!string(context.streams.length));
    auto packetLogger = new PacketLogger(context);
    auto gen = new InputContainer(context, packetLogger, 1);
    gen.loop();
    assert(complete);
}
