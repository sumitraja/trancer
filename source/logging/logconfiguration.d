/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module logging.logconfiguration;
import logging.loglevel;
  
abstract class LogConfiguration {
  LogLevel[string] levels;
  LogLevel defaultLevel;
  
  LogLevel Error;
  LogLevel Warn;
  LogLevel Info;
  LogLevel Trace;
  
  LogLevel getLevel(string name) {
    return levels.get(name, cast(LogLevel)defaultLevel);
  } 
  
  void setLevel(string context, LogLevel level) {
    levels[context] = level;
  }
}

class DefaultLogConfiguration : LogConfiguration {
  
  this() {
    Error = new LogLevel(0, "ERROR");
    Warn = new LogLevel(1, "WARN");
    Info = new LogLevel(2, "INFO");
    Trace = new LogLevel(3, "TRACE");
    defaultLevel = Trace;
  }
}



