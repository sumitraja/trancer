/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module logging.logger;
import std.stdio;
import logging.logconfiguration;
import logging.loglevel;
import std.datetime;

class Logger
{
  static LogConfiguration globalLogConfiguration;
  
  static Logger opCall(string name) {
    if(globalLogConfiguration is null) {
      globalLogConfiguration = new DefaultLogConfiguration;
      writeln("[Logger Main] WARNING Log configuration was not set, using default");
    }
    Logger log = new Logger;
    log.logContext = name;
    return log;
  }
  
  private string logContext;
  
  private void log(lazy string logStatement, const LogLevel level) {
    if(globalLogConfiguration.getLevel(logContext) >= level) {
      doLog(logStatement(), level);
    }
  }
  
  void warn(lazy string logStatement) {
    log(logStatement, globalLogConfiguration.Warn);
  }
  void error(lazy string logStatement) {
    log(logStatement, globalLogConfiguration.Error);
  }
  void info(lazy string logStatement) {
    log(logStatement, globalLogConfiguration.Info);
  }
  void trace(lazy string logStatement) {
    log(logStatement, globalLogConfiguration.Trace);
  }

  void warn(string delegate() logStatement) {
    log(logStatement, globalLogConfiguration.Warn);
  }
  void error(lazy string delegate() logStatement) {
    log(logStatement, globalLogConfiguration.Error);
  }
  void info(lazy string delegate() logStatement) {
    log(logStatement, globalLogConfiguration.Info);
  }
  void trace(lazy string delegate() logStatement) {
    log(logStatement, globalLogConfiguration.Trace);
  }
    
  private void log(string delegate() func, const LogLevel level) {
    if(globalLogConfiguration.getLevel(logContext) >= level) {
      doLog(func(), level);
    }
  }
  
  private void doLog(string statement, const LogLevel level) {
    SysTime currentTime = Clock.currTime(); 
    string timeString = currentTime.toISOExtString();
    writeln("[" ~ timeString ~ "]" ~ "[" ~ level.getName() ~ "][" ~ logContext ~ "] " ~ statement);
  }
  
}




