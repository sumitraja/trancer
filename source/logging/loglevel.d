/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module logging.loglevel;

class LogLevel {
  private const int level;
  private const string name;
  
  this(int level, string name) {
    this.level = level;
    this.name = name;
  }
  
  const string getName() {
    return name;
  }
  
  int opCmp(ref const LogLevel s) const {
    if (level < s.level)  return -1;
    if (level > s.level)  return 1;
    return 0;
  }
}


unittest {
  auto Error = new LogLevel(0, "ERROR");
  auto Warn = new LogLevel(1, "WARN");
  assert(Error < Warn);
  assert(Warn > Error);
  assert(Warn == Warn);
}
