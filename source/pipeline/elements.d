/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module pipeline.elements;

public import std.variant;

struct ProcessorTrampoline {
    ProcessorTrampoline function(ref Variant payload) process;
    bool complete;
}

interface Processor {
	ProcessorTrampoline process(ref Variant payload);
}

class Chained {
	this(ref Processor next) { 
		this.mNext = next; 
	}

	@property Processor next() { 
		return mNext; 
	}

	private Processor mNext;
}

abstract class Pipe : Chained, Processor {

	this(ref Processor next) { 
		super(next);
	}

    final ProcessorTrampoline process(ref Variant payload) {
		auto newPayload = processImpl(payload);
		if(newPayload.hasValue) {
            auto nextTramp = next.process(newPayload);
            if(!nextTramp.complete) {
                return ProcessorTrampoline(nextTramp.process, false);
            }
        } 
        return ProcessorTrampoline(null, true);

	}

protected:
	Variant processImpl(ref Variant payload);
}

abstract class Generator : Chained, Processor {

	this(ref Processor next) { 
        super(next);
	}

    final ProcessorTrampoline process(ref Variant payload) {
        auto pt = next.process(payload);
        while(!pt.complete) {
            pt = pt.process(payload);
        }
        return ProcessorTrampoline(null, true);
    }

    void loop() {
        while(shouldGenerate) {
            auto payload = generate();
            process(payload);
        }
    }

protected:
    Variant generate();
    @property bool shouldGenerate();

}

abstract class Terminator : Processor {

    final ProcessorTrampoline process(ref Variant payload) {
        processImpl(payload);
        return ProcessorTrampoline(null, true);
    }
protected:
    void processImpl(ref Variant payload);
}

unittest {
	import std.conv;
	import std.stdio;
    int count = 5;
	class Gen : Generator {
		this(Processor next) { 
			super(next);
		}

		override Variant generate() {
            return Variant(count--);
		}

        override @property bool shouldGenerate() {
            return count > 0;
        }
	}

	class StringPipe : Pipe {

		this(Processor next) { 
			super(next);
		}
	protected:
        override Variant processImpl(ref Variant payload) {
            if (payload.convertsTo!int()) {
				auto str = Variant(to!string(payload.get!int()));
				return str;
            } else {
                return Variant();
            }
        }
	}

	class DoublerPipe : Pipe {

		this(Processor next) { 
			super(next);
		}

		protected:
        override Variant processImpl(ref Variant payload){
            if (payload.convertsTo!int()) {
                return Variant(payload.get!int() * 2);
            } else {
                return Variant();
            }	
        }
	}

	class Term : Terminator {
        protected override void processImpl(ref Variant payload) {
			assert(payload.get!string() == to!string((count + 1) * 2));
		}
	}
    auto term = new Term();
    auto str = new StringPipe(term);
    auto dbl = new DoublerPipe(str);
	auto gen = new Gen(dbl);
	gen.loop();
    assert(count == 0);
}
