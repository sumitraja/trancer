/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

module pipes.ffmpeg.audio;

import std.conv;
import pipeline.elements;
import utils;
import logging.logger;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import scaffold.ffmpeg.context;
import scaffold.ffmpeg.events;
import pipeline.types;
import generators.ffmpeg.inputcontainer;
import pipes.ffmpeg.decoder;

class AudioDecoder : Decoder {

    this(Processor next) {
        super(next);
        log = Logger("AudioDecoder");
    }

protected:
    override Variant decodePacket(ref PacketEvent event) {
        auto pkt = event.avPacket;
        auto context = event.decoderContextOption.value;
        AVFrame avFrame;
        Variant ret;
        auto stream = context.streamForPacket(pkt);	    
        ubyte[] samples = checkAndAllocateSampleBuffer(pkt);
        int gotFrame = 0;
        /* XXX: could avoid copy if PCM 16 bits with same
         endianness as CPU */
        int dataSize = avcodec_decode_audio4(stream.codecContext, &avFrame, &gotFrame, &pkt);
        if (dataSize < 0) {
            throw new TranscodeException("Failed to decode audio dts=" ~ to!string(stream.avStream.cur_dts), dataSize);
        }
        pkt.data += dataSize;
        pkt.size -= dataSize;

        /* Some bug in mpeg audio decoder gives */
        /* decoded_data_size < 0, it seems they are overflows */
        if (gotFrame > 0) {
            // do this later
            //    ist->next_pts += ((int64_t) AV_TIME_BASE / bps * decoded_data_size)
            //        / (ist->st->codec->sample_rate * ist->st->codec->channels);
            log.trace("Got audio dts=" ~ to!string(pkt.dts) ~ " pts=" ~ to!string(pkt.pts));
            ret = avFrame;
        }
        return ret;
    }

private:
    ubyte[] checkAndAllocateSampleBuffer(ref AVPacket pkt) {
        auto sampleSize = (pkt.size*short.sizeof) > AVCODEC_MAX_AUDIO_FRAME_SIZE ? pkt.size*short.sizeof : AVCODEC_MAX_AUDIO_FRAME_SIZE;
        return new ubyte[sampleSize];
    }

}

unittest {
    av_register_all();
    ulong pts = 0;
    auto packetDecoded = false;

    auto context = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    assert(context.streams.length == 2, to!string(context.streams.length));
    Logger log = Logger("audio-unit-test");

    class FrameLogger : Terminator {
        
        FFmpegTrancerDecoderContext mContext;

        this(FFmpegTrancerDecoderContext context) {
            this.mContext = context;
        }

        override void processImpl(ref Variant payload)  
        in {
            assert(payload.convertsTo!DecodedFrameEvent());
        }
        body {
            assert(payload.get!DecodedFrameEvent().frame.nb_samples == 384, to!string(payload.get!DecodedFrameEvent().frame.nb_samples));
            packetDecoded = true;
        }
    }

    class AudioFilter : Pipe {

        this(Processor next) {
            super(next);
        }

        override Variant processImpl(ref Variant payload)  
        in {
            assert(payload.convertsTo!PacketEvent());
            assert(payload.get!PacketEvent().decoderContextOption.isDefined);
        }
        body {
            auto packetEv = payload.get!PacketEvent();
            auto avPacket = packetEv.avPacket;
            auto context = packetEv.decoderContextOption.value;
            if(context.streamForPacket(avPacket).streamType == StreamType.AUDIO) {
                log.info("Found audio packet on stream " ~ to!string(avPacket.stream_index));
                return payload;
            } else {
                Variant v;
                return v;
            }

        }
    }

    auto decoder = new AudioDecoder(new FrameLogger(context));
    auto filter = new AudioFilter(decoder);
    auto gen = new InputContainer(context, filter, 1);
    gen.loop();
    assert(packetDecoded);
}
