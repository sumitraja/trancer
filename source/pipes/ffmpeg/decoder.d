/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module pipes.ffmpeg.decoder;

import std.conv;
import pipeline.elements;
import utils;
import logging.logger;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import scaffold.ffmpeg.context;
import scaffold.ffmpeg.events;

abstract class Decoder : Pipe {

	this(Processor next) {
		super(next);
	}

	override Variant processImpl(ref Variant payload) 
	in {
		assert(payload.convertsTo!PacketEvent());
        assert(payload.get!PacketEvent().decoderContextOption.isDefined);
	}
	body {
		auto packetEvent = payload.get!PacketEvent();
		auto frame = decodePacket(packetEvent);
		if(frame.hasValue) {
			auto avFrame = frame.get!AVFrame();
            auto avPacket = packetEvent.avPacket;
			frame = Variant(new DecodedFrameEvent(avFrame, avPacket, packetEvent.decoderContextOption, packetEvent.encoderContextOption));
		}
		return frame;
	}
protected:
	Variant decodePacket(ref PacketEvent event);
	Logger log;
	
}
