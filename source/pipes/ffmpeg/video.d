/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 
 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

module pipes.ffmpeg.video;

import std.conv;
import pipeline.elements;
import utils;
import logging.logger;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import scaffold.ffmpeg.context;
import scaffold.ffmpeg.events;
import pipeline.types;
import generators.ffmpeg.inputcontainer;
import pipes.ffmpeg.decoder;

class VideoDecoder : Decoder {
    
    this(Processor next) {
        super(next);
        log = Logger("VideoDecoder");
    }
    
protected:
    
    override Variant decodePacket(ref PacketEvent event) {
        auto pkt = event.avPacket;
        auto context = event.decoderContextOption.value;
        AVFrame avFrame;
        Variant vFrame;
        auto stream = context.streamForPacket(pkt).avStream;	  
        int gotPicture = 0;
        if (pkt.size > 0) {
            int decodedDataSize = (stream.codec.width * stream.codec.height
                                   * 3) / 2;
            /* XXX: allocate picture correctly */
            avcodec_get_frame_defaults(&avFrame);
            
            int ret = avcodec_decode_video2(stream.codec, &avFrame, &gotPicture, &pkt);
            
            if (ret < 0) {
                throw new TranscodeException("Failed to decode video dts=" ~ to!string(stream.cur_dts) ~ " pts=" ~ to!string(stream.pts.val), ret);
            }
            
            if (gotPicture) {
                log.trace("Got picture dts=" ~ to!string(pkt.dts) ~ " pts=" ~ to!string(pkt.pts));
                vFrame = avFrame;
            }
            // deal with this
            //  if (avStream->codec->time_base.num != 0) {
            //    int ticks = ist->st->parser ? ist->st->parser->repeat_pict + 1
            //        : ist->st->codec->ticks_per_frame;
            //    ist->next_pts += ((int64_t) AV_TIME_BASE
            //        * ist->st->codec->time_base.num * ticks)
            //        / ist->st->codec->time_base.den;
            //  }
        }
        return vFrame;
    }
}

unittest {
    av_register_all();
    ulong pts = 0;
    auto packetDecoded = false;
    
    auto context = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    assert(context.streams.length == 2, to!string(context.streams.length));
    Logger log = Logger("video-unit-test");
    
    class FrameLogger : Terminator {
        
        override void processImpl(ref Variant payload)  
        in {
            assert(payload.convertsTo!DecodedFrameEvent());
        }
        body {
            assert(payload.get!DecodedFrameEvent().frame.width == 640, to!string(payload.get!DecodedFrameEvent().frame.width));
            assert(payload.get!DecodedFrameEvent().frame.height == 480, to!string(payload.get!DecodedFrameEvent().frame.width));
            packetDecoded = true;
        }
    }
    
    class VideoFilter : Pipe {
        
        this(Processor next) {
            super(next);
        }
        
        override Variant processImpl(ref Variant payload)  
        in {
            assert(payload.convertsTo!PacketEvent());
            assert(payload.get!PacketEvent().decoderContextOption.isDefined);
        }
        body {
            auto packetEv = payload.get!PacketEvent();
            auto avPacket = packetEv.avPacket;
            auto context = packetEv.decoderContextOption.value;
            if(context.streamForPacket(avPacket).streamType == StreamType.VIDEO) {
                log.info("Found video packet on stream " ~ to!string(avPacket.stream_index));
                return payload;
            } else {
                Variant v;
                return v;
            }
            
        }
    }
    
    auto decoder = new VideoDecoder(new FrameLogger());
    auto filter = new VideoFilter(decoder);
    auto gen = new InputContainer(context, filter, 1);
    gen.loop();
    assert(packetDecoded);
}
