/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

module scaffold.ffmpeg.context;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import ffmpeg.libavutil.samplefmt;
import std.stdio;
import std.string;
import std.conv;
import utils;
import logging.logger;
import scaffold.ffmpeg.stream;
import pipeline.types;

class FFmpegTrancerContextFactory {
    static FFmpegTrancerDecoderContext createDecoderContext(string filename) {
        AVFormatContext* formatContext = avformat_alloc_context();
        auto file_iformat = av_find_input_format(toStringz(filename));
        ffmpegCall({ return avformat_open_input(&formatContext, toStringz(filename), file_iformat, null); });
        log.info(filename ~ " is of format " ~ to!string(formatContext.iformat.name));
        return new FFmpegTrancerDecoderContext(formatContext);
    }

    static FFmpegTrancerEncoderContext createEncoderContext(string formatId, string filename, FFmpegTrancerDecoderContext ic = null) {
        AVFormatContext* formatContext;
        AVDictionary** options;
        ffmpegCall({
            return avformat_alloc_output_context2(&formatContext, cast(AVOutputFormat*) null, toStringz(formatId), toStringz(filename));
        });
        log.info("Opened new container format " ~ to!string(formatContext.oformat.name));
        ffmpegCall({
            return avio_open2(&formatContext.pb, toStringz(filename), AVIO_FLAG_WRITE, &formatContext.interrupt_callback, options);
        });
        log.info("Opened output file " ~ filename);
        return new FFmpegTrancerEncoderContext(formatContext, ic);
    }

private:
    static @property log() {
        if(mLog is null) {
            mLog = Logger("FFmpegTrancerContextFactory");
        }
        return mLog;
    }
    static Logger mLog;
}

class FFmpegTrancerContext {
public:
    this(AVFormatContext* formatContext) {
        mFormatContext = formatContext;
    }

    @property {
        ref AVFormatContext formatContext() {
            return *mFormatContext;
        }

    }

    ~this() {
        if(&mFormatContext !is null) {
            avformat_free_context(mFormatContext);
        }
    }

protected:
    Logger log;
private:
    AVFormatContext* mFormatContext;
}

abstract class FFmpegTrancerStreamContext(T) : FFmpegTrancerContext {
public:

    this(AVFormatContext* formatContext) {
        super(formatContext);
    }
    
    @property {
        ref T[uint] streams() {
            return mStreams;
        }
    }

private:
    T[uint] mStreams;
}

class FFmpegTrancerDecoderContext : FFmpegTrancerStreamContext!(FFmpegDecodingStream) {

public:
    this(AVFormatContext* formatContext) {
        log = Logger("FFmpegTrancerDecoderContext");
        super(formatContext);
        determineStreams();
    }

    @property {
        FFmpegStream streamForPacket(ref AVPacket pkt) {
            int streamIdx = pkt.stream_index;
            auto stream = streamIdx in streams;
            if (stream is null) {
                log.warn("New stream at index " ~ to!string(streamIdx));
                return null;
            } else {
                return *stream;
            }
        }

        StreamType packetType(ref AVPacket pkt) {
            int streamIdx = pkt.stream_index;
            auto stream = streamIdx in streams;
            if (stream is null) {
                log.warn("New stream at index " ~ to!string(streamIdx));
                return StreamType.UNKNOWN;
            }          
            return stream.streamType;
        }
    }

    
private: 
    void determineStreams() {
        AVStream** avStreams = mFormatContext.streams;
        for (int i = 0; i < mFormatContext.nb_streams; i++) {
            AVStream *stream = avStreams[i];
            FFmpegDecodingStream fstream = null;
            switch (stream.codec.codec_type) {
                case AVMediaType.AVMEDIA_TYPE_VIDEO:
                    fstream = new FFmpegDecodingStream(*stream, *mFormatContext, StreamType.VIDEO);
                    log.trace("Found " ~ to!string(fstream.streamType) ~ " of codec " ~ fstream.codec ~ " @ index " ~ to!string(i));
                    break;
                case AVMediaType.AVMEDIA_TYPE_AUDIO:
                    fstream = new FFmpegDecodingStream(*stream, *mFormatContext, StreamType.AUDIO);
                    log.trace("Found " ~ to!string(fstream.streamType) ~ " of codec " ~ fstream.codec ~ " @ index " ~ to!string(i));
                    break;
                case AVMediaType.AVMEDIA_TYPE_DATA:
                case AVMediaType.AVMEDIA_TYPE_NB:
                case AVMediaType.AVMEDIA_TYPE_SUBTITLE:
                case AVMediaType.AVMEDIA_TYPE_UNKNOWN:
                case AVMediaType.AVMEDIA_TYPE_ATTACHMENT:
                default:
                    log.trace("Unknown stream " ~ to!string(stream.codec.codec_type) ~ " has to be ignored");
                    break;
            }
            if(fstream !is null) {
                mStreams[i] =  fstream;
            }

        }
    }
    
}

unittest {
    import std.stdio;
    import std.conv;
    av_register_all();
    ulong pts = 0;
    auto context = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    assert(context.streams.length == 2, to!string(context.streams.length));
    auto stream = context.streams[1];
    assert(stream.streamType == StreamType.AUDIO, to!string(stream.streamType));
    assert(stream.codec == "vorbis", stream.codec);
    stream = context.streams[0];
    assert(stream.streamType == StreamType.VIDEO, to!string(stream.streamType));
    assert(stream.codec == "theora", stream.codec);
}


class FFmpegTrancerEncoderContext : FFmpegTrancerContext {

public:
    this(AVFormatContext* formatContext) {
        log = Logger("FFmpegTrancerEncoderContext");
        super(formatContext);
    }

    this(AVFormatContext* formatContext, FFmpegTrancerDecoderContext dContext) {
        log = Logger("FFmpegTrancerEncoderContext");
        super(formatContext);
        mDecoderContext = dContext;
    }

    @property {
        FFmpegTrancerDecoderContext decoderContext() {
            return mDecoderContext;
        }

    }
    
    FFmpegCopyingStreamStream copyStream(FFmpegDecodingStream source, FFmpegFrameRate frameRate, 
                                         CopyTimeBase copyTimeBase = CopyTimeBase.automatic) {
        auto copy = new FFmpegCopyingStreamStream(source, formatContext, frameRate);
        streams = streams ~ copy;
        return copy;
    }

private:
    FFmpegTrancerDecoderContext mDecoderContext;
    FFmpegWritingStreamStream streams[];

}
