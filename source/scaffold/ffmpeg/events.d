/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module scaffold.ffmpeg.events;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import scaffold.ffmpeg.context;
import utils;

class FFEvent {
    this(Option!FFmpegTrancerDecoderContext dc, Option!FFmpegTrancerEncoderContext ec) {
        decoderContext = dc;
        encoderContext = ec;
    }
private:
    Option!(FFmpegTrancerDecoderContext) decoderContext;
    Option!(FFmpegTrancerEncoderContext) encoderContext;
}

class InputExhaustedEvent : FFEvent {
    this(ref FFmpegTrancerDecoderContext dc) {
        this(new Option!FFmpegTrancerDecoderContext(dc), new Option!FFmpegTrancerEncoderContext());
    }

    this(ref FFmpegTrancerDecoderContext dc, ref FFmpegTrancerEncoderContext ec) {
        this(new Option!FFmpegTrancerDecoderContext(dc), new Option!FFmpegTrancerEncoderContext(ec));
    }
    
    this(Option!FFmpegTrancerDecoderContext dc, Option!FFmpegTrancerEncoderContext ec) {
        super(dc, ec);
    }
}

class PacketEvent : FFEvent {
private:
	AVPacket mPacket;
public:
    this(ref AVPacket pkt) {
        this(pkt, new Option!FFmpegTrancerDecoderContext(), new Option!FFmpegTrancerEncoderContext);
    }

    this(ref AVPacket pkt, ref FFmpegTrancerDecoderContext dc) {
        this(pkt, new Option!FFmpegTrancerDecoderContext(dc), new Option!FFmpegTrancerEncoderContext);
    }

    this(ref AVPacket pkt, ref FFmpegTrancerDecoderContext dc, ref FFmpegTrancerEncoderContext ec) {
        this(pkt, new Option!FFmpegTrancerDecoderContext(dc), new Option!FFmpegTrancerEncoderContext(ec));
	}

    this(ref AVPacket pkt, Option!FFmpegTrancerDecoderContext dc, Option!FFmpegTrancerEncoderContext ec) {
        mPacket = pkt;
        super(dc, ec);
    }

	@property {
        ref AVPacket avPacket() {
            return mPacket;
        }

	    uint index() {
            return mPacket.stream_index;
        }

        Option!FFmpegTrancerDecoderContext decoderContextOption() {
            return decoderContext;
        }

        Option!FFmpegTrancerEncoderContext encoderContextOption() {
            return encoderContext;
        }
    }
}

class DecodedFrameEvent : PacketEvent {
private:
	AVFrame mFrame;

public:
    this(ref AVFrame frame, ref AVPacket pkt, ref FFmpegTrancerDecoderContext dc, ref FFmpegTrancerEncoderContext ec) {
		super(pkt, dc, ec);
		mFrame = frame;
	}

    this(ref AVFrame frame, ref AVPacket pkt, Option!FFmpegTrancerDecoderContext dc, Option!FFmpegTrancerEncoderContext ec) {
        super(pkt, dc, ec);
        mFrame = frame;
    }

    this(ref AVFrame frame, ref AVPacket pkt, ref FFmpegTrancerDecoderContext dc) {
        super(pkt, dc);
        mFrame = frame;
    }

	@property ref AVFrame frame() {
		return mFrame;
	}

}

