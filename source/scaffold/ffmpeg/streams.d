/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * Original code this is based on: Copyright (c) 2000-2003 Fabrice Bellard
 */

module scaffold.ffmpeg.stream;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import ffmpeg.libavutil.samplefmt;
import ffmpeg.libavutil.mem;
import std.stdio;
import std.string;
import std.conv;
import std.stdint;
import utils;
import logging.logger;
import pipeline.types;

alias AVRational FFmpegFrameRate;

class FFmpegStream {

public:
    static const float dtsDeltaThreshold = 10;
    static const float dtsErrorThreshold = 3600*30;
    
    this(ref AVStream avs, ref AVFormatContext fc, StreamType st, ref AVCodec codec) {
        mAVStream = avs;
        mFormatContext = fc;
        mStreamType = st;
        mCodec = codec;
    }

    @property string codec() {
        return to!string(mCodec.name);
    }

    @property const StreamType streamType() {
        return mStreamType;
    }
    
    @property AVCodecContext* codecContext() 
    in {
        assert(mAVStream.codec !is null);
    }
    body {
        return mAVStream.codec;
    }
    
    @property ref AVStream avStream() {
        return mAVStream;
    }
    
    @property const int streamIndex() {
        return mAVStream.index;
    }
    
protected:
    
    void openCodec() {
        ffmpegCall({ 
            return avcodec_open2(mAVStream.codec, &mCodec, null);
        });
        bps = av_get_bytes_per_sample(mAVStream.codec.sample_fmt) >> 3;
        mPTS = 0;
    }

    @property {
        ref AVCodec avCodec() {
            return mCodec;
        }

        ref AVFormatContext formatContext() {
            return mFormatContext;
        }
    }

    Logger log;

private:
    AVStream mAVStream;
    AVFormatContext mFormatContext;
    AVCodec mCodec;
    int bps;
    int mPTS;
    StreamType mStreamType;
}

class FFmpegDecodingStream : FFmpegStream {
    this(ref AVStream avs, ref AVFormatContext fc, StreamType st) {
        log = Logger("FFmpegStream:" ~ to!string(st));
        auto codec = *avcodec_find_decoder(avs.codec.codec_id);
        super(avs, fc, st, codec);
        openCodec();
    }
}

abstract class FFmpegWritingStreamStream : FFmpegStream {
    this(ref FFmpegDecodingStream source, ref AVFormatContext fc, ref AVCodec codecForOutput) {
        auto avs = avformat_new_stream(&fc, null);
        avs.disposition = source.avStream.disposition;
        auto codec = avs.codec;
        auto icodec = source.avStream.codec;
        codec.bits_per_raw_sample = icodec.bits_per_raw_sample;
        codec.chroma_sample_location = icodec.chroma_sample_location;
        super(*avs, fc, source.streamType, codecForOutput);
        avcodec_get_context_defaults3(codec, &codecForOutput);
        this.mSource = source;
        configureCodec();
    }

protected:
    //Configure GLOBAL headers
    // if (oc->oformat->flags & AVFMT_GLOBALHEADER)
    //      c->flags |= CODEC_FLAG_GLOBAL_HEADER;
    void configureCodec();
    @property {
        ref FFmpegDecodingStream source() {
            return mSource;
        }
    }

    
private:
    FFmpegDecodingStream mSource;
}

enum CopyTimeBase : int {
    automatic = -1,
    demuxer = 0,
    decoder = 1,
}
class FFmpegCopyingStreamStream : FFmpegWritingStreamStream {
    this(ref FFmpegDecodingStream source, ref AVFormatContext fc, FFmpegFrameRate frameRate, CopyTimeBase copyTimeBase = CopyTimeBase.automatic) {
        log = Logger("FFmpegCopyingStream:" ~ to!string(source.streamType));
        auto copyCodec = avcodec_find_decoder(source.avCodec.id);
        super(source, fc, *copyCodec);
        this.mCopyTimeBase = copyTimeBase;
        this.mFrameRate = frameRate;
        log.info("Created new copy stream: " ~ to!string(copyCodec.id) ~ " at index " ~ to!string(streamIndex));
    }

protected:
    override void configureCodec() {
        import std.c.stdlib;

        auto icodec = source.avStream.codec;
        auto codec = avStream.codec;
        uint64_t extra_size = cast(uint64_t)icodec.extradata_size + FF_INPUT_BUFFER_PADDING_SIZE;

        if (extra_size > int.max) {
            throw new TranscodeException("Invalid value when configuring codec");
        }

        /* if stream_copy is selected, no need to decode or encode */

        codec.codec_id   = icodec.codec_id;
        codec.codec_type = icodec.codec_type;

        if (!codec.codec_tag) {
            uint codec_tag;
            if (!formatContext.oformat.codec_tag ||
                av_codec_get_id (formatContext.oformat.codec_tag, icodec.codec_tag) == codec.codec_id ||
                !av_codec_get_tag2(formatContext.oformat.codec_tag, icodec.codec_id, &codec_tag))
                codec.codec_tag = icodec.codec_tag;
        }

        codec.bit_rate       = icodec.bit_rate;
        codec.rc_max_rate    = icodec.rc_max_rate;
        codec.rc_buffer_size = icodec.rc_buffer_size;
        codec.field_order    = icodec.field_order;
        codec.extradata      = cast(ubyte*)av_mallocz(extra_size);
        if (!codec.extradata) {
            throw new TranscodeException("Out of memory");
        }
        foreach (i; 0 .. icodec.extradata_size) {
            codec.extradata[i] = icodec.extradata[i];
        }
        codec.extradata_size= icodec.extradata_size;
        codec.bits_per_coded_sample  = icodec.bits_per_coded_sample;

        codec.time_base = source.avStream.time_base;
        /*
         * Avi is a special case here because it supports variable fps but
         * having the fps and timebase differe significantly adds quite some
         * overhead
         */
        if(formatContext.oformat.name == "avi") {
            if ( mCopyTimeBase<0 && av_q2d(source.avStream.r_frame_rate) >= av_q2d(source.avStream.avg_frame_rate)
                && 0.5/av_q2d(source.avStream.r_frame_rate) > av_q2d(source.avStream.time_base)
                && 0.5/av_q2d(source.avStream.r_frame_rate) > av_q2d(icodec.time_base)
                && av_q2d(source.avStream.time_base) < 1.0/500 && av_q2d(icodec.time_base) < 1.0/500
                || mCopyTimeBase==2){
                codec.time_base.num = source.avStream.r_frame_rate.den;
                codec.time_base.den = 2*source.avStream.r_frame_rate.num;
                codec.ticks_per_frame = 2;
            } else if (   mCopyTimeBase<0 && av_q2d(icodec.time_base)*icodec.ticks_per_frame > 2*av_q2d(source.avStream.time_base)
                       && av_q2d(source.avStream.time_base) < 1.0/500
                       || mCopyTimeBase==0){
                codec.time_base = icodec.time_base;
                codec.time_base.num *= icodec.ticks_per_frame;
                codec.time_base.den *= 2;
                codec.ticks_per_frame = 2;
            }
        } else if(!(formatContext.oformat.flags & AVFMT_VARIABLE_FPS)
                  && formatContext.oformat.name == "mov" && formatContext.oformat.name == "mp4" && formatContext.oformat.name == "3gp"
                  && formatContext.oformat.name == "3g2" && formatContext.oformat.name == "psp" && formatContext.oformat.name == "ipod"
                  && formatContext.oformat.name =="f4v") {
            if(mCopyTimeBase<0 && icodec.time_base.den
               && av_q2d(icodec.time_base)*icodec.ticks_per_frame > av_q2d(source.avStream.time_base)
               && av_q2d(source.avStream.time_base) < 1.0/500
               || mCopyTimeBase==0){
                codec.time_base = icodec.time_base;
                codec.time_base.num *= icodec.ticks_per_frame;
            }
        }
        auto tmcdTag = AV_RL32!(['t','m','c','d']);
        if (codec.codec_tag == tmcdTag
            && icodec.time_base.num < icodec.time_base.den
            && icodec.time_base.num > 0
            && icodec.time_base.num > icodec.time_base.den) {
            codec.time_base = icodec.time_base;
        }

        if(mFrameRate.num)
            codec.time_base = av_inv_q(mFrameRate);

        av_reduce(&codec.time_base.num, &codec.time_base.den,
                  codec.time_base.num, codec.time_base.den, int.max);

        switch (codec.codec_type) {
            case AVMediaType.AVMEDIA_TYPE_AUDIO:
                codec.channel_layout     = icodec.channel_layout;
                codec.sample_rate        = icodec.sample_rate;
                codec.channels           = icodec.channels;
                codec.frame_size         = icodec.frame_size;
                codec.audio_service_type = icodec.audio_service_type;
                codec.block_align        = icodec.block_align;
                if((codec.block_align == 1 || codec.block_align == 1152 || codec.block_align == 576) && codec.codec_id == AVCodecID.AV_CODEC_ID_MP3)
                    codec.block_align= 0;
                if(codec.codec_id == AVCodecID.AV_CODEC_ID_AC3)
                    codec.block_align= 0;
                break;
            case AVMediaType.AVMEDIA_TYPE_VIDEO:
                codec.pix_fmt            = icodec.pix_fmt;
                codec.width              = icodec.width;
                codec.height             = icodec.height;
                codec.has_b_frames       = icodec.has_b_frames;
                if (!codec.sample_aspect_ratio.num) {
                    codec.sample_aspect_ratio   =
                        avStream.sample_aspect_ratio =
                            source.avStream.sample_aspect_ratio.num ? source.avStream.sample_aspect_ratio :
                            source.avStream.codec.sample_aspect_ratio.num ?
                            source.avStream.codec.sample_aspect_ratio : AVRational(0, 1);
                }
                avStream.avg_frame_rate = source.avStream.avg_frame_rate;
                break;
            case AVMediaType.AVMEDIA_TYPE_SUBTITLE:
                codec.width  = icodec.width;
                codec.height = icodec.height;
                break;
            case AVMediaType.AVMEDIA_TYPE_DATA:
            case AVMediaType.AVMEDIA_TYPE_ATTACHMENT:
                break;
            default:
                throw new TranscodeException("Unexpected stream type encounted: " ~ to!string(codec.codec_type));
        }
    }
    
private:
    CopyTimeBase mCopyTimeBase;
    FFmpegFrameRate mFrameRate;
}

unittest {

    /**
     * Test a stream copy here
     */

    import std.conv;
    import generators.ffmpeg.inputcontainer;
    import scaffold.ffmpeg.context;
    
    av_register_all();
    ulong pts = 0;
    auto decontext = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    auto stream = decontext.streams[0];
    auto encontext = FFmpegTrancerContextFactory.createEncoderContext("webm", "/tmp/sample.webm", decontext);
    auto subject = encontext.copyStream(stream, FFmpegFrameRate(25,1));
    auto subjectcc = subject.codecContext;
    auto controlcc = stream.codecContext;
    assert(subject.streamType == StreamType.VIDEO);
    assert(controlcc.codec_id == subjectcc.codec_id);
    assert(controlcc.codec_type == subjectcc.codec_type);
    assert(controlcc.bit_rate == subjectcc.bit_rate);
    assert(controlcc.rc_max_rate == subjectcc.rc_max_rate);
    assert(controlcc.rc_buffer_size == subjectcc.rc_buffer_size);
    assert(controlcc.field_order == subjectcc.field_order);
    assert(controlcc.pix_fmt == subjectcc.pix_fmt);
    assert(controlcc.width == subjectcc.width);
    assert(controlcc.height == subjectcc.height);
    assert(controlcc.has_b_frames == subjectcc.has_b_frames);
}
