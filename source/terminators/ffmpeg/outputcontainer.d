/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

module terminators.ffmpeg.outputcontainer;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavutil.avutil;
import ffmpeg.libavformat.avio;
import utils;
import logging.logger;
import pipeline.elements;
import scaffold.ffmpeg.context;
import scaffold.ffmpeg.events;

class OutputContainer : Terminator {

    this(ref FFmpegTrancerEncoderContext context) {
        mContext = context;
        log = Logger("OutputContainer");
    }

    override void processImpl(ref Variant payload)  
    in {
        assert(payload.convertsTo!FFEvent());
    }
    body {
        if(payload.convertsTo!(PacketEvent)) {
            auto avPacket = payload.get!PacketEvent().avPacket;
            writeFrame(avPacket);
        } else if(payload.convertsTo!(InputExhaustedEvent)) {
            close();
        }
    }

private:
    void writeFrame(ref AVPacket pkt) {
        auto avFormatContext = mContext.formatContext;
        if (!headerWritten) {
            avformat_write_header(&avFormatContext, cast(AVDictionary**)null);
            headerWritten = true;
        }
        log.trace({
            import std.format; 
            import std.array;
            auto writer = appender!string(); 
            formattedWrite(writer, "Writing Packet index:%d pts:%d dts:%d", pkt.stream_index, pkt.pts, pkt.dts);
            return writer.data;
        });
        av_interleaved_write_frame(&avFormatContext, &pkt);
    }

    void close() {
        auto avFormatContext = mContext.formatContext;
        av_write_trailer(&avFormatContext);
    }

    Logger log;
    FFmpegTrancerEncoderContext mContext;
    bool headerWritten = false;

}

unittest {
    /**
     * Test a stream copy here
     */

    import std.conv;
    import generators.ffmpeg.inputcontainer;
    import scaffold.ffmpeg.stream;

    av_register_all();
    ulong pts = 0;
    auto decontext = FFmpegTrancerContextFactory.createDecoderContext("samples/sample.ogg");
    auto encontext = FFmpegTrancerContextFactory.createEncoderContext("webm", "/tmp/sample.webm", decontext);
    assert(decontext.streams.length == 2, to!string(decontext.streams.length));
    auto oc = new OutputContainer(encontext);
    auto stream = decontext.streams[1];
    auto subject = encontext.copyStream(stream, FFmpegFrameRate(25,1));
    auto gen = new InputContainer(decontext, oc);
    gen.loop();
}
