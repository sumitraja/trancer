/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
import std.stdio;
import std.string;
import std.conv;
import ffmpeg.libavutil.avutil;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavfilter.avfilter;
import utils;
import logging.logger;
import logging.logconfiguration;

void main(string[] args)
{
  auto log = Logger("Test-Main");

  log.info("AVCodec config: " ~ to!string(avcodec_configuration()));
  log.info("AVCodec licence: " ~ to!string(avcodec_license()));
  log.info("AVFormat config: " ~ to!string(avformat_configuration()));
  log.info("AVFormat licence: " ~ to!string(avformat_license()));
  log.info("AVUtil version: " ~ to!string(avutil_version()));
  log.info("AVFilter version: " ~ to!string(avfilter_version()));

}
