/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module testhelpers;
import ffmpeg.libavutil.avutil;
import ffmpeg.libavcodec.avcodec;
import ffmpeg.libavformat.avformat;
import ffmpeg.libavfilter.avfilter;
import logging.logger;
import logging.logconfiguration;

class TestWiring {
  shared static this () {
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    avfilter_register_all();
    Logger.globalLogConfiguration = new DefaultLogConfiguration;
  }
}
