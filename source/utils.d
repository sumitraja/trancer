/* Copyright (c) 2013-2014 Sumit Raja
 * This file is part of trancer
 * trancer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * trancer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with trancer; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
module utils;
import ffmpeg.libavutil.avutil;
import std.utf;
import std.conv;
import std.string;
import std.algorithm;

class FFmpegCallException : Exception {
  this(string msg, Throwable next = null);
}

class TranscodeException : Exception {
  this(string msg, Throwable next = null) {
    super(msg, next);
  }
  
  this(string msg, int avErrorCode, Throwable next = null) {
    char[] s = new char[1024];
    av_strerror(avErrorCode, cast(char*)s, 1024);
    string builtmsg = msg ~ ": " ~ to!string(s);
    super(builtmsg, next);
  }  
}


void ffmpegCall(int delegate() func) {
  int ret = func();
  if (ret < 0) {
    char[] buffer = new char[1024];
    av_strerror(ret, buffer.ptr, buffer.length);
    throw new Exception(to!string(buffer));
  }
}

int ffmpegCall(int delegate() func, int[] allowedFailures) {
  int ret = func();
  if (ret < 0 && find(allowedFailures, ret) == []) {
    char[] buffer = new char[1024];
    av_strerror(ret, buffer.ptr, buffer.length);
    throw new Exception(to!string(buffer));
  } else {
    return ret;
  }
}

class Option(T) {
  
  private bool mDefined = false;
  private T option;
  private void delegate() destructor = null;
  
  this(ref T option) {
    this(option, null);
    mDefined = true;
  }

  this(ref T option, void delegate() destructor) {
    this.option = option;
    this.destructor = destructor;
    mDefined = true;
  }
    
  this() {
  }
  
  ~this() {
    if(destructor !is null) {
      destructor();
    }
  }
  
  @property T value() {
    if (defined) {
      return option;
    } else {
      throw new Exception("Option is not defined");
    }
  }
  
  @property bool defined() {
    return mDefined;
  }
    
}

unittest {
  class Sample {
    public string value = "string";
  }
  auto subj = new Sample();
  auto opt = new Option!Sample(subj);
  assert(opt.defined == true);
  assert(opt.value is subj);
}

unittest {  
  import std.stdio;
  class Malloced {
    public void* p;
    this() {
      p = std.c.stdlib.malloc(8);
    }
  }

  auto subj2 = new Malloced();
  auto opt = new Option!Malloced(subj2, {std.c.stdlib.free(subj2.p);});
  assert(opt.defined == true);
  assert(opt.value is subj2);
  opt = null;
}

class QualityCollector {

  private long mDroppedFrames = 0;

  private long mDupedFrames = 0;

  private long mVideoSize = 0;

  private int mLastFrameSize = 0;
  
  @property {
    public auto droppedFrames() {
      return mDroppedFrames;
    }
    
    public void droppedFrames(long newValue) {
      mDroppedFrames = newValue;
    }
    
    public auto duplicatedFrames() {
      return mDupedFrames;
    }
    
    public void duplicatedFrames(long newValue) {
      mDupedFrames = newValue;
    }

    public auto videoSize() {
      return mVideoSize;
    }
    
    public void videoSize(long newValue) {
      mVideoSize = newValue;
    }

    public auto lastFrameSize() {
      return mVideoSize;
    }
    
    public void lastFrameSize(int newValue) {
      mLastFrameSize = newValue;
    }
  }
}
